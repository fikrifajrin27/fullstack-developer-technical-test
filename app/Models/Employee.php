<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Employee extends Model
{
    use HasFactory;

    public function getAll(){
        $query = DB::table('employees')
        ->select('employees.*','positions.name as jabatan')
        ->leftJoin('positions','employees.position_id','=','positions.id')
        ->get();

        return json_decode(json_encode($query), true);
    }
}
