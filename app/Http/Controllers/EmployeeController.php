<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use App\Models\Position;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $position = Position::all();
        
        if(request()->ajax()) {
            $employee = Employee::getAll();
            return datatables($employee)
                ->addIndexColumn()
                ->addColumn('action', function($row){
                    $btn = "";
                    $btn .= '<a href="#" onclick="showDetail('.$row['id'].')" class="edit btn btn-primary btn-sm"><i class="fa fa-eye fa-sm" style="padding-right: 0px; font-size:10;"></i></a> ';
                    $btn .= '<a href="#" onclick="show('.$row['id'].')" class="edit btn btn-warning btn-sm"><i class="fa fa-edit fa-sm" style="padding-right: 0px; font-size:10;"></i></a> ';
                    if($row['status'])
                        $btn .= '<a href="#" onclick="updateStatus('.$row['id'].')" class="edit btn text-success btn-sm btn-delete" data-url="" title="Active"><i class="fa fa-toggle-on fa-sm" style="padding-right: 0px; font-size:10;"></i></a>';
                    else
                        $btn .= '<a href="#" onclick="updateStatus('.$row['id'].')" class="edit btn text-danger btn-sm btn-delete" data-url="" title="Inactive"><i class="fa fa-toggle-off fa-sm" style="padding-right: 0px; font-size:10;"></i></a>';

                    return $btn;
                })
                ->rawColumns(['action'])
                ->toJson();
        }

        $data = [
            "position" => $position
        ];
        return view('employee', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nip' => 'required|unique:employees',
            'nama' => 'required',
            'fotoKtp' => 'mimes:jpeg,png'
        ]);

        // return $request;

        if($request->hasFile('fotoKtp')) {
            $uploadPath = public_path('uploads/ktp_pegawai');
            if(!File::isDirectory($uploadPath)) {
                File::makeDirectory($uploadPath, 0755, true, true);
            }            
            $file = $request->file('fotoKtp');
            $explode = explode('.', $file->getClientOriginalName());
            $originalName = $explode[0];
            $extension = $file->getClientOriginalExtension();
            $org_rename = 'ktp_' . date('YmdHis');
            $rename = $org_rename . '.' . $extension;
            $file->move($uploadPath, $rename);
        }
        
        $employee = new Employee();
        $employee->position_id = $request->jabatan;
        $employee->nama = $request->nama;
        $employee->nip = $request->nip;
        $employee->departemen = $request->departemen;
        $employee->tanggal_lahir = $request->tanggal_lahir;
        $employee->tahun_lahir = $request->tahun_lahir;
        $employee->alamat = $request->alamat;
        $employee->nomor_telepon = $request->telepon;
        $employee->agama = $request->agama;
        $employee->status = $request->status == "true" ? true : false;
        $employee->foto_ktp = $rename;
        $employee->save();
        
        return [
            "status" => "200",
            "message" => "Karyawan sukses ditambahkan"
        ];    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Employee::find($id);

        $position = Position::find($employee->position_id);
        $employee->position_name = $position->name;
        $employee->path_ktp = asset('/uploads/ktp_pegawai'.'/'.$employee->foto_ktp);

        return $employee;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request ,Employee $employee)
    {
        return $request;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee = Employee::find($id);
        $employee->position_id = $request->jabatan;
        $employee->nama = $request->nama;
        $employee->nip = $request->nip;
        $employee->departemen = $request->departemen;
        $employee->tanggal_lahir = $request->tanggal_lahir;
        $employee->tahun_lahir = $request->tahun_lahir;
        $employee->alamat = $request->alamat;
        $employee->nomor_telepon = $request->telepon;
        $employee->agama = $request->agama;

        if($request->hasFile('fotoKtpEdit')) {
            $uploadPath = public_path('uploads/ktp_pegawai');
            if (file_exists( $uploadPath . '/' . $employee->foto_ktp)) {
                unlink($uploadPath . '/' . $employee->foto_ktp);
            }
            $file = $request->file('fotoKtp');
            $explode = explode('.', $file->getClientOriginalName());
            $originalName = $explode[0];
            $extension = $file->getClientOriginalExtension();
            $org_rename = 'ktp_' . date('YmdHis');
            $rename = $org_rename . '.' . $extension;
            $file->move($uploadPath, $rename);
            $employee->foto_ktp = $rename;
        }
        $employee->save();

        return [
            "status" => "200",
            "message" => "Karyawan berhasil diupdate"
        ];    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $employee = Employee::find($id);
        $status = $employee->status;
        $employee->status = $status ? false : true;
        $employee->save();
        return true;
    }
}
