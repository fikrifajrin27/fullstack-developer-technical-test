@extends('layout.app')

@section('css')
  <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
  <link href="{{ asset('vendor/bootstrap-datepicker/css/bootstrap-datepicker.css') }}" rel="stylesheet">
@endsection
@section('breadcrumb')
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Karyawan</h1>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="page">Karyawan</li>
    </ol>
</div>
@endsection
@section('content')
<div class="card sm mb-4">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary"></h6>
    </div>
    <div class="card-body">
        <div class="table-responsive p-3">
            <div class="row">
                <div class="col-lg-12">
                    <button type="button" class="btn btn-primary mb-4 float-right" data-toggle="modal" data-target="#tambahModal">
                      Tambah Karyawan
                    </button>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <table class="table align-items-center table-flush" id="dataTable">
                      <thead class="thead-light">
                        <tr>
                          <th>No.</th>
                          <th>Name</th>
                          <th>Jabatan</th>
                          <th>Departemen</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                    </table>
                </div>
            </div>

          </div>
    </div>
</div>

<div class="modal fade" id="tambahModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Baru Karyawan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="addSubmit" enctype="multipart/form-data">
            <div class="form-group">
                <label for="formGroupExampleInput">Nama Karyawan</label>
                <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan Nama Karyawan Baru" onkeydown="return /(?![0-9])./gmi.test(event.key)">
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">NIP</label>
                <input type="text" class="form-control" id="nip" name="nip" placeholder="Masukkan NIP Karyawan Baru">
                <span class="text-danger" id="invalidNip"></span>
            </div>
            <div class="form-group">
                <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Jabatan</label>
                <select class="custom-select my-1 mr-sm-2" id="jabatan" name="jabatan">
                    <option value="" selected>Pilih Jabatan Karyawan...</option>
                    @foreach($position as $k => $v)
                        <option value="{{ $v->id }}">{{ $v->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">Departemen</label>
                <input type="text" class="form-control" id="departemen" name="departmen" placeholder="Masukkan Departmen Karyawan">
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">Tanggal Lahir</label>
                <input type="text" class="form-control" id="tanggal_lahir" name="tanggal_lahir" placeholder="Masukkan Tanggal Lahir Karyawan">
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">Tahun Lahir</label>
                <input type="text" class="form-control" id="tahun_lahir" name="tahun_lahir" placeholder="Masukkan Tahun Lahir Karyawan">
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">Alamat</label>
                <textarea class="form-control" id="alamat" name="alamat" rows="3"></textarea>
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">Nomor Telepon</label>
                <input type="text" class="form-control" id="telepon" name="telepon" placeholder="Masukkan Nomor Telepon Karyawan" oninput="this.value=this.value.replace(/(?![0-9])./gmi,'')">
            </div>
            <div class="form-group">
                <legend class="col-form-label col-sm-2" style="padding-left: 0px;">Agama</legend>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="agama" id="agama" value="Islam">
                    <label class="form-check-label" for="inlineRadio1">Islam</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="agama" id="agama" value="Protestan">
                    <label class="form-check-label" for="inlineRadio1">Protestan</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="agama" id="agama" value="Katolik">
                    <label class="form-check-label" for="inlineRadio1">Katolik</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="agama" id="agama-hindu" value="Hindu">
                    <label class="form-check-label" for="inlineRadio1">Hindu</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="agama" id="agama" value="Buddha">
                    <label class="form-check-label" for="inlineRadio1">Buddha</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="agama" id="agama" value="Khonghucu">
                    <label class="form-check-label" for="inlineRadio1">Khonghucu</label>
                </div>
            </div>
            <div class="form-group">
                <legend class="col-form-label col-sm-12" style="padding-left: 0px;">Status Karyawan</legend>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="status" id="status" value="true" checked="checked">
                    <label class="form-check-label" for="inlineRadio1">Aktif</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="status" id="staus" value="false">
                    <label class="form-check-label" for="inlineRadio1">Tidak Aktif</label>
                </div>
            </div>
            <div class="form-group">
                <div class="custom-file mb-3">
                    <input type="file" class="custom-file-input" id="fotoKtp">
                    <label class="custom-file-label" for="validatedCustomFile">Upload file scan KTP...</label>
                    <span class="text-danger" id="invalidFotoKtp"></span>
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="showDetail" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Detail Karyawan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <div id="detailKtp"></div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-lg-12 ml-4">
                <table width="100%">
                    <tr>
                        <td style="width:30%;">Nama</td>
                        <td style="width:70%;">
                            <div id="detailNama"></div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:30%;">NIP</td>
                        <td style="width:70%;">
                            <div id="detailNip"></div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:30%;">Jabatan</td>
                        <td style="width:70%;">
                            <div id="detailJabatan"></div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:30%;">Departemen</td>
                        <td style="width:70%;">
                            <div id="detailDepartemen"></div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:30%;">Tanggal Lahir</td>
                        <td style="width:70%;">
                            <div id="detailTglLahir"></div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:30%;">Tahun Lahir</td>
                        <td style="width:70%;">
                            <div id="detailTahunLahir"></div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:30%;">Alamat</td>
                        <td style="width:70%;">
                            <div id="detailAlamat"></div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:30%;">Nomor Telepon</td>
                        <td style="width:70%;">
                            <div id="detailNoTlp"></div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:30%;">Agama</td>
                        <td style="width:70%;">
                            <div id="detailAgama"></div>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:30%;">Status</td>
                        <td style="width:70%;">
                            <div id="detailStatus"></div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Karyawan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="editSubmit" enctype="multipart/form-data">
            <input type="hidden" id="karyawanIdEdit">
            <div class="form-group">
                <label for="formGroupExampleInput">Nama Karyawan</label>
                <input type="text" class="form-control" id="namaEdit" name="namaEdit" placeholder="Masukkan Nama Karyawan Baru" onkeydown="return /(?![0-9])./gmi.test(event.key)">
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">NIP</label>
                <input type="text" class="form-control" id="nipEdit" name="nipEdit" placeholder="Masukkan NIP Karyawan Baru">
                <span class="text-danger" id="invalidNipEdit"></span>
            </div>
            <div class="form-group">
                <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Jabatan</label>
                <select class="custom-select my-1 mr-sm-2" id="jabatanEdit" name="jabatanEdit">
                    <option value="">Pilih Jabatan Karyawan...</option>
                    @foreach($position as $k => $v)
                        <option value="{{ $v->id }}">{{ $v->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">Departemen</label>
                <input type="text" class="form-control" id="departemenEdit" name="departmenEdit" placeholder="Masukkan Departmen Karyawan">
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">Tanggal Lahir</label>
                <input type="text" class="form-control" id="tanggal_lahirEdit" name="tanggal_lahirEdit" placeholder="Masukkan Tanggal Lahir Karyawan">
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">Tahun Lahir</label>
                <input type="text" class="form-control" id="tahun_lahirEdit" name="tahun_lahirEdit" placeholder="Masukkan Tahun Lahir Karyawan">
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">Alamat</label>
                <textarea class="form-control" id="alamatEdit" name="alamatEdit" rows="3"></textarea>
            </div>
            <div class="form-group">
                <label for="formGroupExampleInput2">Nomor Telepon</label>
                <input type="text" class="form-control" id="teleponEdit" name="teleponEdit" placeholder="Masukkan Nomor Telepon Karyawan" oninput="this.value=this.value.replace(/(?![0-9])./gmi,'')">
            </div>
            <div class="form-group">
                <legend class="col-form-label col-sm-2" style="padding-left: 0px;">Agama</legend>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="agamaEdit" id="agamaEdit" value="Islam">
                    <label class="form-check-label" for="inlineRadio1">Islam</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="agamaEdit" id="agamaEdit" value="Protestan">
                    <label class="form-check-label" for="inlineRadio1">Protestan</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="agamaEdit" id="agamaEdit" value="Katolik">
                    <label class="form-check-label" for="inlineRadio1">Katolik</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="agamaEdit" id="agamaEdit" value="Hindu">
                    <label class="form-check-label" for="inlineRadio1">Hindu</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="agamaEdit" id="agamaEdit" value="Buddha">
                    <label class="form-check-label" for="inlineRadio1">Buddha</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="agamaEdit" id="agamaEdit" value="Khonghucu">
                    <label class="form-check-label" for="inlineRadio1">Khonghucu</label>
                </div>
            </div>
            <div class="form-group">
                <div class="custom-file mb-3">
                    <input type="file" class="custom-file-input" id="fotoKtpEdit">
                    <label class="custom-file-label" for="validatedCustomFile">Upload file scan KTP...</label>
                    <span class="text-danger" id="invalidFotoKtpEdit"></span>
                </div>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      </form>
    </div>
  </div>
</div>


@endsection

@push('js')
<script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('vendor/sweetalert2/dist/sweetalert.min.js') }}"></script>
<!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
<script>

    $( document ).ready(function() {
        reloadTable();
    });

    function reloadTable(){
        $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            bDestroy: true,
            ajax: "{{ route('employee.index') }}",
            columns: [
                { data: 'DT_RowIndex', orderable: false, searchable: false, className: "text-center", width:'10%'},
                { data: 'nama', width:'30%'},
                { data: 'jabatan', width:'20%'},
                { data: 'departemen', width:'20%'},
                { data: 'action', orderable: false, searchable: false, className: "text-center", width:'20%' }
            ]
        })
    }

    $('#tanggal_lahir').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
    });

    $("#tahun_lahir").datepicker( {
        format: "yyyy", // Notice the Extra space at the beginning
        viewMode: "years", 
        minViewMode: "years",
        autoclose: true,
    });
    
    $('#tanggal_lahirEdit').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
    });

    $("#tahun_lahirEdit").datepicker( {
        format: "yyyy", // Notice the Extra space at the beginning
        viewMode: "years", 
        minViewMode: "years",
        autoclose: true,
    });

    $('#addSubmit').on('submit',function(e){
        e.preventDefault();
        // console.log(e);

        let nama = $('#nama').val();
        let nip = $('#nip').val();
        let jabatan = $('#jabatan').val();
        let departemen = $('#departemen').val();
        let tanggal_lahir = $('#tanggal_lahir').val();
        let tahun_lahir = $('#tahun_lahir').val();
        let alamat = $('#alamat').val();
        let telepon = $('#telepon').val();
        let agama = $('input[name="agama"]:checked').val();
        let status = $('input[name="status"]:checked').val();
        let fotoKtp = $('#fotoKtp')[0].files[0];
        
        let fd = new FormData();
        fd.append('nama', nama);
        fd.append('nip', nip);
        fd.append('jabatan', jabatan);
        fd.append('departemen', departemen);
        fd.append('tanggal_lahir', tanggal_lahir);
        fd.append('tahun_lahir', tahun_lahir);
        fd.append('alamat', alamat);
        fd.append('telepon', telepon);
        fd.append('agama', agama);
        fd.append('status', status);
        fd.append('fotoKtp', fotoKtp);

        $.ajax({
            url: "employee",
            type:"POST",
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            data:fd,
            success:function(response){
                if (response.status == '200') {
                    $('#addSubmit')[0].reset();
                    $('#tambahModal').modal('toggle');
                    reloadTable();
                    swal("Berhasil Disimpan!", "Karyawan Berhasil Disimpan", "success");
                }
            },
            error: function(response) {
                $('#invalidNip').text(response.responseJSON.errors.nip);
                // $('#emailErrorMsg').text(response.responseJSON.errors.email);
                // $('#mobileErrorMsg').text(response.responseJSON.errors.mobile);
                $('#invalidFotoKtp').text(response.responseJSON.errors.fotoKtp);
            },
        });
    });
    
    $('#editSubmit').on('submit',function(e){
        e.preventDefault();
        // console.log(e);

        let idKaryawan = $('#karyawanIdEdit').val();
        let nama = $('#namaEdit').val();
        let nip = $('#nipEdit').val();
        let jabatan = $('#jabatanEdit').val();
        let departemen = $('#departemenEdit').val();
        let tanggal_lahir = $('#tanggal_lahirEdit').val();
        let tahun_lahir = $('#tahun_lahirEdit').val();
        let alamat = $('#alamatEdit').val();
        let telepon = $('#teleponEdit').val();
        let agama = $('input[name="agamaEdit"]:checked').val();
        let fotoKtp = $('#fotoKtp')[0].files[0];
        
        let fd = new FormData();
        fd.append('_method', "PUT");
        fd.append('nama', nama);
        fd.append('nip', nip);
        fd.append('jabatan', jabatan);
        fd.append('departemen', departemen);
        fd.append('tanggal_lahir', tanggal_lahir);
        fd.append('tahun_lahir', tahun_lahir);
        fd.append('alamat', alamat);
        fd.append('telepon', telepon);
        fd.append('agama', agama);
        fd.append('fotoKtp', fotoKtp);

        var url = "{{ route('employee.update', ":id") }}";
        url = url.replace(':id', idKaryawan);
        $.ajax({
            url: url,
            type:"POST",
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': "{{ csrf_token() }}"
            },
            data:fd,
            success:function(response){
                console.log(response);
                if (response.status == '200') {
                    $('#editSubmit')[0].reset();
                    $('#editModal').modal('toggle');
                    reloadTable();
                    swal("Berhasil Disimpan!", "Data Karyawan Berhasil Disimpan", "success");
                }
            },
            error: function(response) {
                $('#invalidNipEdit').text(response.responseJSON.errors.nip);
                // $('#emailErrorMsg').text(response.responseJSON.errors.email);
                // $('#mobileErrorMsg').text(response.responseJSON.errors.mobile);
                $('#invalidFotoKtpEdit').text(response.responseJSON.errors.fotoKtp);
            },
        });
    });

    function showDetail(id) {
        event.preventDefault();
        var fd = new FormData();
		fd.append('id', id);

        var url = "{{ route('employee.show', ":id") }}";
        url = url.replace(':id', id);
        $.ajax({
			type: "GET",
			url: url,
			data: fd,
            processData: false,
            contentType: false,
			success: function(result) {
                console.log(result);
                $("#detailKtp").html('<center><img src="' + result.path_ktp + '" width="420px" height="250px"  /></center>');
                $("#detailNama").html(result.nama);
                $("#detailNip").html(result.nip);
                $("#detailJabatan").html(result.position_name);
                $("#detailDepartemen").html(result.departemen);
                $("#detailTglLahir").html(result.tanggal_lahir);
                $("#detailTahunLahir").html(result.tahun_lahir);
                $("#detailAlamat").html(result.alamat);
                $("#detailNoTlp").html(result.nomor_telepon);
                $("#detailAgama").html(result.agama);
                $("#detailStatus").html(result.status? "Aktif" : "Non Aktif");
                $('#showDetail').modal('show'); 
			}
		});
        
    }
    
    function show(id) {
        event.preventDefault();
        var fd = new FormData();
		fd.append('id', id);

        var url = "{{ route('employee.show', ":id") }}";
        url = url.replace(':id', id);
        $.ajax({
			type: "GET",
			url: url,
			data: fd,
            processData: false,
            contentType: false,
			success: function(result) {
                $('#karyawanIdEdit').val(result.id);
                $('#namaEdit').val(result.nama);
                $('#nipEdit').val(result.nip);
                $('#jabatanEdit').val(result.position_id);
                $('#departemenEdit').val(result.departemen);
                $('#tanggal_lahirEdit').val(result.tanggal_lahir);
                $('#tahun_lahirEdit').val(result.tahun_lahir);
                $('#alamatEdit').val(result.alamat);
                $('#teleponEdit').val(result.nomor_telepon);
                // $('input[name="agamaEdit"]:checked').val(result.agama);
                $("input[name=agamaEdit][value=" + result.agama + "]").attr('checked', 'checked');
                $('#editModal').modal('show'); 
			}
		});
        
    }

    function updateStatus(id){
        event.preventDefault();
        swal({
            title: "Apakah Kamu Yakin?",
            text: "Status karyawan akan berubah",
            icon: "warning",
            buttons: [
                'Tidak, batalkan!',
                'Ya, tentu!'
            ],
            dangerMode: true,
            }).then(function(isConfirm) {
            if (isConfirm) {
                var urldestroy = "{{ route('employee.destroy', ":id") }}";
                urldestroy = urldestroy.replace(':id', id);
                $.ajax({
                    type: "DELETE",
                    url: urldestroy,
                    processData: false,
                    contentType: false,
                    headers: {
                        'X-CSRF-TOKEN': "{{ csrf_token() }}"
                    },
                    success: function(result) {
                        swal({
                            title: 'Sukses!',
                            text: 'Status karyawan berhasil diubah!',
                            icon: 'success'
                        });
                        reloadTable();
                    }
                });
            } else {
                swal("Dibatalkan", "Status karyawan tidaak berubah :)", "error");
            }
        })
    }
</script>
@endpush
