<?php

use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\PositionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});

Route::post('employee', 'EmployeeController@store')->name('employee.store');

Route::get('employee/show/{id}', 'EmployeeController@show')->name('employee.show');

Route::delete('/employee/destroy/{id}',[EmployeeController::class, 'destroy'])->name('employee.destroy');
Route::put('/employee/{id}',[EmployeeController::class, 'update'])->name('employee.update');

Route::resource('position',PositionController::class);
Route::resource('employee',EmployeeController::class);
